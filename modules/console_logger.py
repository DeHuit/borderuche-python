class Logger :

    __module_name = "Unknown"
    def __init__(self, module_name):
        self.__module_name = module_name

    def error(self, text):
        print("[ERROR] [" + self.__module_name + "] : " + text)


    def notify(self, text) :
        print("[NOTIFY] [" + self.__module_name + "] : " + text)

    def warning(self, text) :
        print("[WARNING] [" + self.__module_name + "] : " + text)


if __name__ == '__main__':
    l = Logger("Test")
    l.error("text")
    l.notify("text")
    l.warning("text")