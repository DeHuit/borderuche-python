# import picamera
import time
import fcntl
# import numpy
import cv2
import os
import sys
from console_logger import Logger


class Cameras:
    """
    This module is designed to take manage cameras, connected to RPi (including PiCam) as a single module.
    After taking photos, module automatically stores taken photos into local storage.
    """

    __log = Logger("Cameras") #Logger to print errors to a console

    __path = "." # path to storage
    __storage_name = "camera_storage" #storage name

    __cam_list = [] # list of camera indexes
    __cam_descriptors = [] # list of camera descriptors

    def __init__(self, camera_range=10):
        self.init_cameras(camera_range)
        self.create_new_storage()

    ######## INIT/DESCTRUCT CAMERAS ###########
    def init_cameras(self, camera_range):
        """
        Initialise cameras. Called automatically on creation of the instance.
        :param camera_range: maximal number of connected cameras
        :return: list of indexes of found supported cameras
        """
        # Cleanup camera descriptors if present
        self.close_all()

        self.__log.notify("****************************")
        self.__log.notify("Verbose output is normal. It is caused by the nature of some libraries and can't be muted.")
        # Trying to open cameras
        # camera_range is doubled because some camera may be doubled in several (eventually unsupported) formats
        for i in range(0, camera_range*2):
            cam = cv2.VideoCapture(i)
            if cam.isOpened():
                self.__log.notify("Camera #" + str(i) + " detected")
                self.__cam_list.append(i)
                self.__cam_descriptors.append(cam)
        self.__log.notify("****************************")
        self.__log.notify("Cameras detected :" + str(self.__cam_list))
        return self.__cam_list

    def get_available_cameras(self):
        """
        Get indexes of available cameras
        :return: list of cameras index
        """
        return self.__cam_list.copy()

    def ___get_camera_descriptors(self):
        """
        Returns list of camera descriptors. Dont use this function if you dont know what it is.
        :return: list of cv2 objects containing cameras
        """
        return self.__cam_descriptors.copy()

    def close_camera(self, index):
        """
        Close and remove camera from module
        :param index: camera index
        :return: True if was successful, False if not
        """
        if index in self.__cam_list :
            i = self.__cam_list.index(index)
            self.__cam_descriptors[i].release()
            del self.__cam_descriptors[i]
            self.__cam_list.remove(index)
            return True
        else :
            return False

    def close_all(self) :
        '''
        Close all available cameras.
        :return: None
        '''
        for cam in self.__cam_descriptors:
            cam.release()
        self.__cam_descriptors = []
        self.__cam_list = []

    ############### OPERATION ON CAMERAS ###############

    def read_image(self, index):
        '''
        Take image and return it in cv2 array format
        :param index: camera index
        :return: image as an array or None
        '''
        if index not in self.__cam_list:
            self.__log.error("Camera " + str(index) + " is not present")
            return None
        cam = self.__cam_descriptors[self.__cam_list.index(index)]
        cam.set(3, 3280)
        cam.set(4, 2464)
        was_taken, frame = cam.read()
        if not was_taken:
            return None
        return frame

    def take_picure(self, index):
        '''
        Takes and saves a picture to a disc from camera
        :param index: camera index
        :return: String with path to a picture or None if can't take a picture
        '''
        if index not in self.__cam_list:
            self.__log.error("Camera " + str(index) + " is not present")
            return None
        # relative path to storage as /path/[storage]/cam_x/
        path = self.__path + "/" + self.__storage_name + "/cam_" + str(index)
        if not os.path.exists(path):
            os.mkdir(path)
        name = path + "/" + str(index) + "_" + time.strftime("%Y%m%d-%H%M%S")  + ".jpg"
        cam = self.__cam_descriptors[self.__cam_list.index(index)]
        cam.set(3, 3280)
        cam.set(4, 2464)
        was_taken, frame = cam.read()
        if not was_taken:
            self.__log.error("Camera is unable to take a picture")
            return None
        self.__log.notify(name + ".jpg is created")
        cv2.imwrite(name, frame)
        return name

    __stream_active = False

    def stream(self, num):
        '''
        Open stream from the camera
        :param num: camera index
        :return: True if stream started, False if not
        '''
        if self.__stream_active :
            self.__log.error("You can't stream 2 cameras at the time")
        if num in self.__cam_list:
            self.__stream_active = True
            cam = self.__cam_descriptors[self.__cam_list.index(num)]
            # Better quality may be unstable
            cam.set(3, 640)
            cam.set(4, 480)
            while True:
                self.__log.notify("CAmera found")
                was_taken, frame = cam.read()
                cv2.imshow("Stream. Press any key to quit...", frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            cv2.destroyAllWindows()
            self.__stream_active = False
        else:
            self.__log.error("Camera #" + str(num) + "doe not exists")
            return None

    def burst_pics(self):
        '''
        Take and save picture from every camera
        :return: None5
        '''
        for i in self.__cam_list:
            self.take_picure(i)

    ############# STORAGE SECTION ######################

    def get_storage_path(self):
        """
        Returns full storage path as /.../path/[storage_name]
        :return: string with storage path
        """
        return os.path.abspath(self.__path + "/" + self.__storage_name)

    def set_storage_path(self, new_path):
        """
        Set new path to storage
        :param new_path: new path to storage (absolute or relative)
        :return: None
        """
        self.__path = new_path
        self.create_new_storage()


    def set_storage_name(self, new_name):
        self.__storage_name = new_name
        self.create_new_storage()

    def create_new_storage(self):
        # Checking path
        if not os.path.exists(self.__path):
            os.makedirs(self.__path)
        # Creating storage
        storage = self.__path + "/" + self.__storage_name
        if not os.path.exists(storage):
            os.mkdir(storage)
            self.__log.notify("Storage created on " + storage)
        else:
            self.__log.notify("Storage is present")
        # Creating folder for each camera
        for i in self.__cam_list:
            f_name = storage + "/cam_" + str(i)
            if not os.path.exists(f_name):
                os.mkdir(f_name)


if __name__ == "__main__":
    # execute only if run as a script
    c = Cameras()
    print("pics")
    c.burst_pics()
    #c.stream(0)
    c.close_all()
