import smbus
import time


class SHT :
    
    __bus = None
    __address = 0x44

    def __init__(self, bus, address = 0x44):
        self.__address = address
        self.__bus = bus
        self.__bus.write_i2c_block_data(self.__address, 0x2C, [0x06])
        time.sleep(0.5)
     
    def read(self) :
        """
        Get data from sensor
        :return: pair (temperature, humidity)
        """
        data = self.__bus.read_i2c_block_data(self.__address, 0x00, 6)
        temp = data[0] * 256 + data[1]
        cTemp = -45 + (175 * temp / 65535.0)
        humidity = 100 * (data[3] * 256 + data[4]) / 65535.0
        return (cTemp, humidity)
    
s = SHT(smbus.SMBus(1))
print(s.read())