import smbus
import time
from modules.arduino.pkg import Package, ARD_COMMANDS as acmd
from modules.arduino.c_lib.converter import *
from modules.console_logger import Logger
from modules.arduino.led import *
from modules.arduino.scales import *
"""
This class is a set of function that allows Raspberry Pi to commincate with Arduino via I2C and mini embedded protocol
"""
class Arduino :
    #################
    # SETUP SECTION #
    # Depending on which arduino/STM32/other chip you are using you may need
    # to define some of these parameters
    __int_size = 2 # size of integer in byte
    __double_size = 4 # size of double in byte
    __float_size = 4 # # size of integer in byte
    # END SETUP SECTION #
    #####################

    __log = Logger("Arduino")

    __bus = None
    __address = 0x12

    __buffer = [] #last package received
    MAX_ATTEMPTS_NB = 5

    # Public parameters
    scales = None
    led = None

    def __init__(self, bus, address=0x12):
        self.__address = address
        self.__bus = bus
        self.scales = Scales(self)
        self.led = LED(self)

    # Setup
    def set_address(self, new_adr):
        """
        Set arduino address on I2C bus
        :param new_adr:
        :return:
        """
        __address = new_adr

    def check_connection(self) :
        """
        Check if arduino is connected via I2C
        :return: true if arduino is connected, false if not
        """
        try :
            self.__bus.write_byte(self.__address, acmd.IGNORE.value)
            return True
        except :
            self.__log.error("Arduino is not detected")
            return False

    def send_command(self, cmd):
        """
        Send command to via I2C
        :param cmd: command to send
        :return: data array with feedback or None in case of error (see error message)
        """
        p = Package()
        try :
            l = self.__bus.read_i2c_block_data(self.__address, cmd.value)
            p.build_from_list(l)
            if p.check_integrity():
                return p
            else :
                self.__log.error("Data is corrupted")
                return None
        except Exception as e:
            self.__log.error(str(e))
            return None

    def send_data(self, cmd, data):
        """
        Send a data associated to a command to Arduino
        :param cmd: command to execute
        :param data: data to transmit
        :return: arduino feedback (value int(1)) in case of success or None in case of fail (see error message)
        """
        p = Package()
        p_l = p.build(cmd, data)
        # Syntax : write(address, cmd (package 1st byte), data (remaining package))
        self.__bus.write_i2c_block_data(self.__address, p_l[0], p_l[1:])
        time.sleep(0.1)
        return self.send_command(acmd.Q_GET_FEEDBACK)

    def get_last_action(self):
        """
        Get last task performed by Arduino task manager
        :return: ARD_COMMAND with task or ARD_COMMAND.R_UNKNOWN in case of failure
        """
        try:
            data = self.send_command(acmd.Q_GET_LAST_TASK).get_data()
            return acmd(data[0])
        except :
            return acmd.R_UNKNOWN

    def get_current_action(self):
        """
        Get current task that is performed by Arduino task manager
        :return: ARD_COMMAND with task or ARD_COMMAND.R_UNKNOWN in case of failure
        """
        try :
            ret = self.send_command(acmd.Q_GET_CURRENT_TASK)
            if ret == None :
                return None
            data = ret.get_data()
            return acmd(data[0])
        except :
            return acmd.R_UNKNOWN