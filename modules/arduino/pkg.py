from enum import Enum

def crc8(message):
    CRC7_POLY = 0x91 # CRC Polynome. Should be the same on the both sides
    crc = 0x00
    for i in range (0, len(message)) :
        crc ^= message[i]
        for j in range (0,8) :
            if (crc & 1) :
                crc ^= CRC7_POLY
            crc >>= 1
    return crc

class ARD_COMMANDS(Enum) :
    # Getters for arduino states
    Q_GET_FEEDBACK = 0x02
    Q_GET_CURRENT_TASK = 0x03
    Q_GET_LAST_TASK = 0x04

    # Arduino states
    STATE_IDLE = 0x08

    # Other functions
    IGNORE = 0xFF

#/**
# * Services
# * 1x - scales
# * 2x - LED light
# * 3x - gates
# * 4x - temperature sensors
# */
    ## QUERIES ##
    Q_SCALES_GET_WEIGHT = 0x11
    Q_SCALES_GET_RAW  = 0x12
    Q_SCALES_SET_CORRECTION_FACTOR = 0x13
    Q_SCALES_TARE = 0x14
    Q_LIGHT_SET_DAY = 0x21
    Q_LIGHT_SET_NIGHT = 0x22
    Q_LIGHT_GET_STATE = 0x23
    Q_GET_GATE_COUNT = 0x31
    Q_GET_GATE_ALL = 0x32
    Q_RESET_GATE_COUNT = 0x33
    Q_RESET_ALL = 0x34
    Q_TEMPERATURE_GET = 0x41

    ## RESPONSES : x* + 70 ##
    R_OFFSET = 0x70
    R_GET_FEEDBACK = 0x72
    R_GET_CURRENT_TASK = 0x73
    R_GET_LAST_TASK = 0x74
    R_SCALES_GET_WEIGHT = 0x81
    R_SCALES_GET_RAW = 0x82
    R_SCALES_SET_CORRECTION_FACTOR = 0x83
    R_SCALES_TARE = 0x84
    R_LIGHT_SET_DAY = 0x91
    R_LIGHT_SET_NIGHT = 0x92
    R_LIGHT_GET_STATE = 0x93
    R_TEMPERATURE_GET = 0xB1
    R_UNKNOWN = 0xFF


class Package :

    '''
    - Mini protocol was built around I2C transmission. Package is a byte array that follows the structure :
    - -----------------------------------......----------------------
    - |     CMD    | PACKAGE SIZE |       DATA       |     CRC7     |
    - -----------------------------------......----------------------
    - Where
    - 1) CMD byte - is one of the foloowing R_* or Q_* commands
    - 2) PACKAGE_SIZE byte- is size of whole package in bytes, including header and CRC (so data size is PACKAGE_SIZE - 3)
    - 3) DATA - is sequence of bytes with some content (this section may be empty)
    - 4) CRC7 byte - is hashsum on package (is calculated for whole package)
    '''

    __array = []

    def __init__(self):
        self.__array = "\0"

    def build (self, cmd, data) :
        self.__array = []
        self.__array.append(cmd.value)
        self.__array.append(3 + len(data))
        self.__array.extend(data)
        self.__array.append(crc8(self.__array))
        return self.__array

    def build_from_list(self, list):
        del (self.__array)
        self.__array = list[0:list[1]].copy()
    
    def as_list(self):
        return self.__array.copy()
    
    def destroy(self) :
        del(self.__array)

    def get_cmd(self):
        if (len(self.__array) > 1) :
            return self.__array[0]
        else :
            return -1

    def get_size(self):
        if (len(self.__array) > 1) :
            return len(self.__array[1])
        else :
            return -1

    def get_data(self):
        if (len(self.__array) > 2) :
            return self.__array[2:-1]
        else :
            return -1
        
    def get_data_size(self) :
        return self.get_size() - 3
        
    def check_integrity(self):
        return self.__array[-1] == crc8(self.__array[0:-1])
