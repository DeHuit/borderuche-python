from modules.arduino.arduino import *
from modules.console_logger import *
from modules.arduino.c_lib.converter import *
from modules.arduino.pkg import *
############## SETTERS FOR ARDUINO PARAMETERS ###############
class Scales :

    __log = Logger("Arduino/SCALES")
    __ard = None

    def __init__(self, arduino):
        self.__ard = arduino

    def tare(self):
        """
        Send command to arduino to tare
        :return: True in case of success, False in case of failure
        """
        try:
            res = self.__ard.send_command(acmd.Q_SCALES_TARE)
            if res == None:
                return None
            return res.get_cmd() == acmd.R_SCALES_TARE.value
        except Exception as e:
            self.__log.error(str(e))
            return False


    def set_correction_factor(self, corr):
        """
        Send to arduino command to set new correction factore for scales
        :param corr: new correction factor
        :return: True in case of success, False in case of failure
        """
        try:
            float_tab = float_to_chars(corr, self.__ard.__float_size)
            res = self.__ard.send_data(acmd.Q_SCALES_SET_CORRECTION_FACTOR, float_tab)
            if res == None:
                return None
            return res.get_data()[0] == acmd.R_SCALES_SET_CORRECTION_FACTOR.value
        except Exception as e:
            self.__log.error(str(e))
            return False


    ################### GETTERS FOR SCALE PARAMETERS #########################

    def get_weight(self):
        """
        Get weight from Arduino scales
        :return: weight in gr
        """
        tmp = self.__ard.send_command(acmd.Q_SCALES_GET_WEIGHT)
        if tmp == None:
            return None
        ws = []
        for i in range(0, 4 * self.__ard.__int_size, self.__ard.__int_size):
            ws.extend(chars_to_int(tmp.get_data()[i:i + self.__ard.__int_size], self.__ard.__int_size))
        return ws


    def get_raw_gain(self):
        """
        Get raw gain as is from arduino scales
        :return: float containing raw gain
        """
        ret = self.__ard.send_command(acmd.Q_SCALES_GET_RAW)
        tab = ret.get_data()
        rg = []
        for i in range(0, 4 * self.__ard.__float_size, self.__ard.__float_size):
            rg.extend(chars_to_float(tab[i:i + self.__ard.__float_size], self.__ard.__float_size))
        return rg