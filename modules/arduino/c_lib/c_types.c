#include <stdio.h>
#include <string.h>
// To compile library gcc -shared -Wl,-soname,adder -o adder.so -fPIC add.c


int chars_to_int(char * chars, int int_size) {
    switch (int_size) {
		case 2 : {  short int tmp = 0; // 16 bit int
					memcpy(&tmp, chars, 2);
					int tmp_int = tmp;
					return tmp_int; }
		case 4 : {  int tmp = 0; // 32 bit int
					memcpy(&tmp, chars, 4);
					return tmp; }
		default : return -1;
	}
}

float chars_to_float(char * chars, int float_size) {
    
    switch (float_size) {
		case 4 : {  float tmp = 0; // 4 bytes long
					memcpy(&tmp, chars, 4);
					return tmp; }
		case 8 : {  long tmp = 0; // 8 bytes long
					memcpy(&tmp, chars, 8);
					return tmp; }
		default : return -1;
	}
}

double chars_to_double(char * chars, int double_size) {

    switch (double_size) {
		case 4 : {  float tmp = 0; // 4 bytes long
					memcpy(&tmp, chars, 4);
					double tmp_double =tmp;
					return tmp_double; }
		case 8 : {  double tmp = 0; // 8 bytes long
					memcpy(&tmp, chars, 8);
					return tmp; }
		default : return -1;
	}
}

long chars_to_long(char * chars, int long_size) {
    switch (long_size) {
		case 4 : {  int tmp = 0; // 4 bytes long
					memcpy(&tmp, chars, 4);
					long tmp_long =tmp;
					return tmp_long; }
		case 8 : {  long tmp = 0; // 8 bytes long
					memcpy(&tmp, chars, 8);
					return tmp; }
		default : return -1;
	}
}

int int_to_chars(int in, char * out, int int_size){
    memcpy(out, &in, int_size);
    return int_size;
}

int float_to_chars(float in, char * out, int float_size){
    memcpy(out, &in, float_size);
    return float_size;
}

int double_to_chars(double in, char * out, int double_size){
    switch (double_size) {
		case 4 : {  float tmp = in; // 4 bytes long
					memcpy(out, &tmp, 4);
					break;
				 }
		case 8 : {  memcpy(out, &in, 8); // 8 bytes long
					break; }
		default : return -1;
	}
	return double_size;
}

int long_to_chars(long in, char * out, int long_size){
    memcpy(out, &in, long_size);
    return (int) long_size;
}
