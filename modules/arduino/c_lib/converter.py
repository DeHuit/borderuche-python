from ctypes import *
import struct
import os

__lib = None
if not os.getcwd().find("c_lib") == -1 :
    __lib = CDLL("./ct.so")
else :
    __lib = CDLL("./arduino/c_lib/ct.so")


######################
# Typisation of functions' parameters.
######################

    #chars_to_int function
__lib.chars_to_int.restype = c_int
__lib.chars_to_int.argtypes = [c_char_p, c_int]

__lib.chars_to_float.restype = c_float
__lib.chars_to_float.argtypes = [c_char_p, c_int]

__lib.chars_to_double.restype = c_double
__lib.chars_to_double.argtypes = [c_char_p, c_int]

__lib.chars_to_long.restype = c_long
__lib.chars_to_long.argtypes = [c_char_p, c_int]

__lib.int_to_chars.restype = c_int
__lib.int_to_chars.argtypes = [c_int, c_char_p]

__lib.float_to_chars.restype = c_int
__lib.float_to_chars.argtypes = [c_float, c_char_p]

__lib.double_to_chars.restype = c_int
__lib.double_to_chars.argtypes = [c_double, c_char_p]

__lib.long_to_chars.restype = c_int
__lib.long_to_chars.argtypes = [c_long, c_char_p]

__mode = "B" #mode of bitewise representation

def set_lib_location(path) :
    __lib = path

###############
## Chars to *
###############

def chars_to_int(chars, int_size) :
    """
    Convert char array from C to Python int
    :param chars: pointer to char array
    :param int_size: size of int (should be copied from Arduino.__int_size)
    :return: integer value of it's byte representation
    """
    c = create_string_buffer(int_size)
    for i in range(0, int_size) :
        c[i] = struct.pack(__mode, chars[i])
    i = __lib.chars_to_int(c, c_int(int_size))
    return i

def chars_to_float(chars, float_size) :
    """
    Convert char array from C to Python float
    :param chars: pointer to char array
    :param float_size: size of float (should be copied from Arduino.__float_size)
    :return: float value of it's byte representation
    """
    c = create_string_buffer(float_size)
    for i in range(0, float_size) :
        c[i] = struct.pack(__mode, chars[i])
    f = __lib.chars_to_float(c, c_int(float_size))
    return f

def chars_to_double(chars, double_size) :
    """
    Convert char array from C to Python double
    :param chars: pointer to char array contain double
    :param double_size: double size in bytes (should be copied from Arduino.__double_size)
    :return: double value
    """
    c = create_string_buffer(double_size)
    for i in range(0, double_size):
        c[i] = struct.pack(__mode, chars[i])
    d = __lib.chars_to_double(c, c_int(double_size))
    return d

def chars_to_long(chars, long_size) :
    """
    Convert char array from C to Python long
    :param chars: pointer to char array containing double
    :param long_size: long size (Arduino.__long_size)
    :return: long value
    """
    c = create_string_buffer(long_size)
    for i in range(0, long_size) :
        c[i] = struct.pack(__mode, chars[i])
    l = __lib.chars_to_long(c, c_int(long_size))
    return l

###############
## * to chars
###############

def int_to_chars(int, int_size) :
    """
    Convert Python int to it's byte representation in C
    :param int: int value
    :param int_size: int size in target architecture
    :return: list of bytes
    """
    f = c_int(int)
    c = create_string_buffer(int_size)
    __lib.int_to_chars(f, c, c_int(int_size))
    list = []
    list.extend(c.raw)
    return list

def float_to_chars(float, float_size) :
    """
    Convert Python float to it's byte representation in C
    :param float: float value
    :param float_size: float size in target architecture
    :return: list of bytes
    """
    f = c_float(float)
    c = create_string_buffer(float_size)
    __lib.float_to_chars(f, c, c_int(float_size))
    list = []
    list.extend(c.raw)
    return list

def double_to_chars(double, double_szie) :
    """
    Convert Python double (or float) to it's byte representation in C
    :param double: double value
    :param double_szie: double value in target architecture
    :return: list of bytes
    """
    d = c_double(double)
    c = create_string_buffer(double_szie)
    __lib.double_to_chars(d, c, c_int(double_szie))
    list = []
    list.extend(c.raw)
    return list

def long_to_chars(long, long_size) :
    """
    Convert Python long to it's byte representation in C
    :param long: long (or int) value
    :param long_size: long value in target architecture
    :return: byte list
    """
    f = c_long(long)
    c = create_string_buffer(long_size)
    __lib.long_to_chars(f, c, c_int(long_size))
    list = []
    list.extend(c.raw)
    return list