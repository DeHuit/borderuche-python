from modules.console_logger import *
from modules.arduino.arduino import *
from modules.arduino.pkg import *

class LED :
    __log = Logger("Arduino/LED")
    __ard = None

    def __init__(self, arduino):
        self.__ard = arduino


    def set_day(self):
        """
        Send command to arduino to set day light
        :return: True in case of success, False in case of failure
        """
        try:
            res = self.__ard.send_command(acmd.Q_LIGHT_SET_DAY)
            if res == None:
                return False
            return res.get_cmd() == acmd.R_LIGHT_SET_DAY.value
        except Exception as e:
            self.__log.error(str(e))
            return False


    def set_night(self):
        """
        Send command to arduino to set night light
        :return: True in case of success, False in case of failure
        """
        try:
            res = self.__ard.send_command(acmd.Q_LIGHT_SET_NIGHT)
            if res == None:
                return False
            return res.get_cmd() == acmd.R_LIGHT_SET_NIGHT.value
        except Exception as e:
            self.__log.error(str(e))
            return False

    def get_daytime(self) :
        """
        Get from arduino current state of the light
        :return: ARD_COMMANDS with daytime or None in case of error
        """
        try :
            ret = self.__ard.send_command(acmd.Q_LIGHT_GET_STATE)
            if ret == None :
                return None
            return ret.get_data()[0]
        except Exception as e:
            self.__log.error(str(e))
            return None