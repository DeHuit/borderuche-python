import RPi.GPIO as GPIO
import time

class Stepper_motor :
    __position = 0
    __pins = []
    __step = 0
    __dir = 0
    def __init__(self, in_one = 6, in_two = 13 , in_three = 19, in_four = 26):
        self.__pins = [in_one, in_two, in_three, in_four]
        for pin in self.__pins :
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.LOW)

    def __one_step(self, dir):
        if dir :
            GPIO.output(self.__pins[0], GPIO.HIGH if (self.__step == 0) else GPIO.LOW)
            GPIO.output(self.__pins[1], GPIO.HIGH if (self.__step == 1) else GPIO.LOW)
            GPIO.output(self.__pins[2], GPIO.HIGH if (self.__step == 2) else GPIO.LOW)
            GPIO.output(self.__pins[3], GPIO.HIGH if (self.__step == 3) else GPIO.LOW)
        else :
            GPIO.output(self.__pins[0], GPIO.HIGH if (self.__step == 3) else GPIO.LOW)
            GPIO.output(self.__pins[1], GPIO.HIGH if (self.__step == 2) else GPIO.LOW)
            GPIO.output(self.__pins[2], GPIO.HIGH if (self.__step == 1) else GPIO.LOW)
            GPIO.output(self.__pins[3], GPIO.HIGH if (self.__step == 0) else GPIO.LOW)
        self.__step = (self.__step + 1) % 4

    def turn_45(self, dir) :
        for i in range(0,256) :
            self.__one_step(dir)
            time.sleep(0.005)

    def turn_90(self, dir):
        for i in range(0, 512):
            self.__one_step(dir)
            time.sleep(0.005)

    def turn_180(self, dir):
        for i in range(0, 1024):
            self.__one_step(dir)
            time.sleep(0.005)

if __name__ == '__main__' :
    GPIO.setmode(GPIO.BCM)
    st = Stepper_motor()
    st.turn_90(1)
    st.turn_90(0)