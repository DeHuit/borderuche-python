
# On importe Tkinter
from tkinter import *
#from picamera import PiCamera
#from PIL import Image
from modules.arduino.arduino import *

ard = Arduino()

IMAGE_PATH = "/home/pi/Desktop/image.jpg"
last_image = None
#camera = PiCamera()

# On crée une fenêtre, racine de notre interface
fenetre = Tk()

# On crée un label (ligne de texte) souhaitant la bienvenue
# Note : le premier paramètre passé au constructeur de Label est notre
# interface racine
v = StringVar()
v.set("Cik to get weight")
champ_label = Label(fenetre, textvariable=v)

var_choix = StringVar()
var_choix.set("Click to know")
def change_txt() :
    var_choix.set(str(ard.scales_get_weight()) + " gr")
    v.set(var_choix.get())

# On affiche le label dans la fenêtre
champ_label.pack()

day_b = Button(fenetre, text="Set day", command=ard.light_set_day)
day_b.pack()

night_b = Button(fenetre, text="Set night", command=ard.light_set_night)
night_b.pack()

photo_b = Button(fenetre, text="Get wieght", command=change_txt)
photo_b.pack()


# On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
fenetre.mainloop()
