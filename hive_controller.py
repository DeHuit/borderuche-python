import smbus
from modules.counter import *
from modules.cameras import *
from modules.sht import *
from modules.steppermotor import *
from modules.arduino.arduino import *


class HiveController:
    bus = smbus.SMBus(1)

    sht = None
    motor =  None
    counter = None
    cams = None
    arduino = None

    def i2c_scan(self) :
        device_list = []
        for device in range(128) :
            try :
                self.bus.read_byte(device)
                device_list.append(hex(device))
            except :
                pass
        return device_list

    def initialize_default(self) :
        self.sht = SHT(self.bus)
        self.motor = Stepper_motor()
        self.counter = Counter()
        self.cams = Cameras()
        self.arduino = Arduino(bus=self.bus)