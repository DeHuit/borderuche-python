import board
import busio
from digitalio import Direction, Pull
from adafruit_mcp230xx.mcp23017 import *
import time as t
from RPi import GPIO


class Counter :
    
    __DEBOUNCE_TIME = 100
    __PASSAGE_TIMEOUT_MIN = 200
    __PASSAGE_TIMEOUT_MAX = 1500
    
    counters = None
    
    def __init__(self, addr = 0x20, interrupt_pin=4) :
        
        # Setting up arduino interrumptions
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(interrupt_pin, GPIO.IN, GPIO.PUD_UP) # Set up Pi's pin as input, pull up
        GPIO.add_event_detect(interrupt_pin, GPIO.FALLING, callback=self.__print_interrupt, bouncetime=1)
        
        self.i2c = busio.I2C(board.SCL, board.SDA)
        self.pins = []
        self.mcp = MCP23017(self.i2c, address=addr)
        
        for pin in range(0, 16) :
            p = self.mcp.get_pin(pin)
            self.pins.append(p)
            p.direction = Direction.INPUT
            p.pull = Pull.UP
        
        self.enable_interrupts()
        self.reset()

    ints = 0
    def __print_interrupt(self,port):
        '''Callback function to be called when an Interrupt occurs.'''
        for pin_flag in self.mcp.int_flag:
            p = pin_flag
            v = self.pins[p].value
            self.mcp.clear_ints()
            
            old_v = self.pin_hist[p]
            self.pin_hist[p] = v
            if not (old_v and not v) : # only "rising edge" interruption passes
                return
            self.ints += 1
            current_gate = self.gates_time_queue[p]
            opposite_gate = self.gates_time_queue[(p+8)%16]
            time = int(round(t.time() * 1000))
            
            if len(current_gate) > 0 and time - current_gate[0] < self.__DEBOUNCE_TIME :
                return
            
            entrence_time = 0
            while len(opposite_gate) > 0 and time - entrence_time > self.__PASSAGE_TIMEOUT_MAX :
                entrence_time = opposite_gate.pop()
                
            if (time - entrence_time) > self.__PASSAGE_TIMEOUT_MAX :
                current_gate.append(time)
                return
            elif (time - entrence_time) < self.__PASSAGE_TIMEOUT_MIN :
                opposite_gate.insert(0, entrence_time)
                return
            
            self.counters[p] += 1
        
    def reset(self) :
        self.counters = [0 for i in range(0, 16)]
        self.gates_time_queue = [[] for i in range (0, 16)]
        self.pin_hist = [True for i in range(0, 16)]
        self.mcp.clear_ints()
    
    def enable_interrupts(self) :
        # Set up to check all the port B pins (pins 8-15) w/interrupts!
        self.mcp.interrupt_enable = 0xFFFF        # Enable Interrupts in all pins
        # If intcon is set to 0's we will get interrupts on
        # both button presses and button releases
        self.mcp.interrupt_configuration = 0x0000         # interrupt on any change
        self.mcp.io_control = 0x44   # Interrupt as open drain and mirrored
        #self.mcp.clear_ints()        # Interrupts need to be cleared initially
        
    def disable_interrupts(self) :
        self.mcp.interrupt_enable = 0x0000
        self.mcp.clear_ints()
        
    def get_counters(self) :
        return self.counters.copy()
    
    def get_gate(self, number) :
        if not (0 < number and number < 16) :
            return (-1, -1)
        IN = self.counters[number] if number < 8 else self.counters[number -8]
        OUT = self.counters[number+8] if number < 8 else self.counters[number]
        return (IN, OUT)

try :
    c = Counter()
    while True :
        print(c.get_counters(), " ", c.ints)
        t.sleep(1)
except (KeyboardInterrupt, SystemExit):
    print('Bye :)')

finally:
    GPIO.cleanup()